# Terraform doesn't allow specifying required providers yet, but this
# is placed here in case it is allowed:
# https://github.com/hashicorp/terraform/issues/17191
provider "aws" {
  region = var.aws_region
  profile = var.aws_profile
  alias = "main"
  version = "~> 2.9"
}

provider "aws" {
  region = "us-east-1"
  profile = var.aws_profile
  alias = "cloudfront"
  version = "~> 2.9"
}


