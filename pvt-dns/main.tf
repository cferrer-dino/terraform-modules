# Create private zone for VPC
resource "aws_route53_zone" "private_dns" {
  name = "${var.organization}.${var.domain}"
  comment = "private zone for ${var.organization} zone in ${var.prefix} env"

  lifecycle {
    ignore_changes = [vpc]
  }


  vpc {
    vpc_id = var.vpc_id
  }
  tags = merge(
          var.tags,
          {
            "Organization" = var.organization
            "Environment" = var.prefix
            "Terraform" = "true"
            }
          )
}

resource "aws_route53_zone_association" "secondary" {
  count   = var.secondary_association == "true" ? 1 : 0
  zone_id = aws_route53_zone.private_dns.zone_id
  vpc_id  = var.vpc_dns_id
}