variable "aws_profile" {
  description = "The AWS profile to use (e.g. default)"
  type        = string
}

variable "aws_region" {
  description = "Default Region"
  default = "us-east-1"
}

variable "organization" {
  description = "Organization"
  type = string
  default = ""
}

variable "prefix" {
  description = "Prefix name"
  type = string
  default = ""
}

variable "domain" {
  description = "Domain name"
  type = string
  default = ""
}

variable "vpc_id" {
  description = "VPC ID for Private DNS Hosted Zone"
  default = ""
}

variable "vpc_dns_id" {
  description = "VPC DNS ID for Attachment and use for cross aws resolving"
  default = ""
}

variable "tags" {
  description = "Tags"
  type = map(string)
  default = {}
}

variable "secondary_association" {
  description= "Enable secondary association"
  type= string
  default = "true"
  