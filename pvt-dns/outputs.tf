output "private_zone_id" {
  value = aws_route53_zone.private_dns.id
}

output "private_zone_name_servers" {
  value = aws_route53_zone.private_dns.name_servers
}

